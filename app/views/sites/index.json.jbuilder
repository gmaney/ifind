json.array!(@sites) do |site|
  json.extract! site, :id, :name, :address, :city, :state, :zip, :phone, :contact, :url, :facebook, :twitter, :macs, :pcs, :sunday, :monday, :tuesday, :wedndesday, :thursday, :friday, :saturday, :holidays, :languages, :lat, :lon
  json.url site_url(site, format: :json)
end
