class MainController < ApplicationController
  def index
  
  end

  def distance(site,lat,lng) 
  	Math.sqrt((site.lat - lat)**2 + (site.lon - lng)**2) 
  end
  def found
  	require 'geocoder'
  	address = params['street'] + ' ' + params['city'] + ' ' + params['state'] + ' ' + params['zip'] 
	where = Geocoder.search(address).first
	lat = where.geometry['location']['lat']
	lng = where.geometry['location']['lng']
	closest = Site.all.sort_by {|x| distance(x,lat,lng)}.take(3)
	google = Geocoder::Lookup::Google.new
	@listings = []
	closest.each do |close|
		miles = distance(close,lat,lng)	* 100
		map = google.map_link_url([close.lat, close.lon])
		@listings[@listings.size] = {'site' => close, 'miles' => miles, 'map' => map}
	end
  end
end
