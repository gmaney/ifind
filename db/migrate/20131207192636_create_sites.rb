class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :name
      t.string :address
      t.string :city
      t.string :state
      t.string :zip
      t.string :phone
      t.string :contact
      t.string :url
      t.string :facebook
      t.string :twitter
      t.integer :macs
      t.integer :pcs
      t.string :sunday
      t.string :monday
      t.string :tuesday
      t.string :wedndesday
      t.string :thursday
      t.string :friday
      t.string :saturday
      t.text :holidays
      t.text :languages
      t.float :lat
      t.float :lon

      t.timestamps
    end
  end
end
