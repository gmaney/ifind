# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'geocoder'

File.open('db/libraries.tsv').readlines.each do | line |
	fields = line.split("\t")
	address = "#{fields[1]} San Francisco, California #{fields[2]}"
	where = Geocoder.search(address).first
	next if where == nil
	lat = where.geometry['location']['lat']
	lng = where.geometry['location']['lng']
	site = Site.new
	site.name = fields[0]
	site.address = fields[1]
	site.city = 'San Francisco'
	site.state = 'California'
	site.zip = fields[2]
	site.lat = lat
	site.lon = lng 
	site.save
end